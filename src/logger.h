#ifndef LOGGER_H
#define LOGGER_H

#include <osapi.h>

#include "constants.h"

#define log(group, msg, ...) {\
    static int8_t prev_group = GR_UNKNOWN;\
    os_printf(S_NEW_LINE);\
    if (group != prev_group) {\
        os_printf(S_DELIMETER);\
        os_printf(S_NEW_LINE);\
    }\
    os_printf(msg, ##__VA_ARGS__);\
    prev_group = group;\
}

#endif //LOGGER_H
