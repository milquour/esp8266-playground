#include "debug.h"

static void uptime_timer_callback(void* args) {
    static uint32_t count = 0;

    digitalWrite(LED_BLINK_PIN, count%2);
    int time = LED_BLINK_DELAY*(++count);
    log(GR_DEBUG, "time: %3d:%02d:%02d.%03d",\
        time/(60*60*1000),\
        (time/(60*1000))%60,\
        (time/1000)%60,\
        time%1000);
}

void debug_write_char(char c) {
    uart_write_char(_uart_debug, c);
}

void debug_setup(void) {
    #if DEBUG_ENABLED
        _uart_debug = uart_init(UART0, DEBUG_BAUDRATE, UART_8N1, UART_FULL, 1);
        os_install_putc1((void*)debug_write_char);
        
        pinMode(LED_BLINK_PIN, OUTPUT);
        digitalWrite(LED_BLINK_PIN, HIGH);
        
        os_timer_disarm(&_uptime_timer);
        os_timer_setfn(&_uptime_timer, (os_timer_func_t*)uptime_timer_callback, (void*)0);
        os_timer_arm(&_uptime_timer, LED_BLINK_DELAY, 1);
    #endif
}
