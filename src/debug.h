#ifndef DEBUG_H
#define DEBUG_H
#define DEBUG_ENABLED 1

#include <Arduino.h>
#include <ets_sys.h>
#include <user_interface.h>
#include <osapi.h>
#include <os_type.h>

#include "constants.h"
#include "logger.h"

static ETSTimer _uptime_timer;
static uart_t* _uart_debug;

static void uptime_timer_callback(void* args);
void debug_write_char(char c);
void debug_setup(void);

#endif //DEBUG_H
