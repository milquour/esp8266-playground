#ifndef CONSTANTS_H
#define CONSTANTS_H

// message groups
#define GR_UNKNOWN  -1
#define GR_CONFIG   1
#define GR_DEBUG    2
// end message groups

// messages
#define S_NEW_LINE              "\r\n"
#define S_DELIMETER             "------------------------"
#define MSG_ESP_CONFIG_START    "--ESP config start------"
#define MSG_ESP_CONFIG_END      "--ESP config end--------"
// end messages

// debug options
#define LED_BLINK_DELAY 30*1000 //ms
#define LED_BLINK_PIN 2
#define DEBUG_BAUDRATE 115200
// end debug options

#endif //CONSTANTS_H
